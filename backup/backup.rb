#!/usr/bin/env ruby

require 'getoptlong'

def parse_line
  opts = GetoptLong.new(
    [ '--help', '-h', GetoptLong::NO_ARGUMENT ],
    [ '--zip', '-z', GetoptLong::NO_ARGUMENT ],
    [ '--unzip', '-u', GetoptLong::NO_ARGUMENT ],
    [ '--path', '-p', GetoptLong::REQUIRED_ARGUMENT ],
    [ '--name', '-n', GetoptLong::REQUIRED_ARGUMENT ]
  )
  args = {}
  opts.each do |opt, arg|
    case opt
      when '--help'
        show_help
      when '--zip'
        error('Укажите точно, что вы хотите делать. Архивировать или разархивировать.') if args[:mode] == :unzip
        args[:mode] = :zip
      when '--unzip'
        error('Укажите точно, что вы хотите делать. Архивировать или разархивировать.') if args[:mode] == :zip
        args[:mode] = :unzip
      when '--path'
        args[:path] = arg
      when '--name'
        args[:name] = arg
      end
    end
  args
end

def show_help
  puts 'Для работы скрипта необходимо указать следующие параметры:
  --path путь до корневой директории проекта
  --name путь до бэкап-архива.
  Указать способ работы:
  -u распаковать бэкап,
  -z запаковать бэкап'
  exit 0
end

def error(error_msg)
  puts error_msg
  exit 0
end

def prepare
  checked_run('apt-get', 'install', '-y', 'tar')
end

def checked_run(*args, dir: nil)
  command = args.join(' ')
  if !dir.nil?
    command = "cd #{dir} && #{command}"
  end
  puts "Running #{command}"
  system command
end

def script(args)
  path_to_dir = File.expand_path(args[:path])
  path_to_zip = File.expand_path(args[:name])
  if args[:mode] == :zip
    zip(path_to_dir, path_to_zip)
  else
    unzip(path_to_dir, path_to_zip)
  end
end

def zip(path_to_dir, path_to_zip)
  checked_run('rm', '-r', '-f', '/tmp/backup')
  checked_run('mkdir', '/tmp/backup')
  checked_run('cp', '-r', File.join(path_to_dir, 'gogs', 'base'), '/tmp/backup/gogs') 
  checked_run('cp', '-r', File.join(path_to_dir, 'taiga', 'postgres_data'), '/tmp/backup/postgres')
  checked_run('tar', '-cf', path_to_zip, '/tmp/backup')
  checked_run('rm', '-r', '-f', '/tmp/backup')
end

def unzip(path_to_dir, path_to_zip)
  checked_run('rm', '-r', '-f', '/tmp/backup')
  checked_run('tar', '-xvf', path_to_zip, '-C', '/')
  checked_run('rm', '-r', File.join(path_to_dir, 'gogs', 'base'))
  checked_run('rm', '-r', File.join(path_to_dir, 'taiga', 'postgres_data'))
  checked_run('cp', '-r', '/tmp/backup/gogs', File.join(path_to_dir, 'gogs', 'base'))
  checked_run('cp', '-r', '/tmp/backup/postgres', File.join(path_to_dir, 'taiga', 'postgres_data'))
  checked_run('rm', '-r', '-f', '/tmp/backup')
end

if __FILE__ == $0
  args = parse_line
  error('Неправильное число аргументов, смотреть --help') if args.size != 3
  pp args
  prepare
  script(args)  
end
