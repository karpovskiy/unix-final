# Описание Dockerfile для сервиса strider_cd

Контейнер основан на образе `ubuntu:focal`
```
FROM ubuntu:focal
```
Переменная `DEBIAN_FRONTEND=noninteractive` нужня для отключения интерактивного режима.
```
ENV DEBIAN_FRONTEND=noninteractive
```
Переменная `DB_URI` - адрес к базе данных MongoDB.
```
ENV DB_URI=mongodb+srv://karpovskiy:karpovskiy@cluster0-thxqs.mongodb.net/db?retryWrites=true&w=majority
```
Установка зависимостей.
```
RUN apt-get update && \
  apt-get install -y git python3-pip curl  mongodb
```
Установка `nodejs`.
```
RUN curl -sL https://deb.nodesource.com/setup_12.x | bash -
RUN apt-get install -y nodejs 
```
Установка `ruby` и `bundler` для того, что бы можно было использовать `ruby`-проекты.
```
RUN apt-get install -y ruby ruby-all-dev
RUN gem install bundler
```
Установка дополнительной зависимости для компиляции приложения.
```
RUN npm install -g node-gyp
```
Добавление пользователя, ссылки и репозитория для `strider`
```
RUN mkdir -p /home/strider && mkdir -p /opt/strider
RUN adduser --disabled-password --gecos "" --home /home/strider strider
RUN chown -R strider:strider /home/strider
RUN chown -R strider:strider /opt/strider
RUN ln -s /opt/strider/src/bin/strider /usr/local/bin/strider
USER strider
```
Установка пременной окружения, которая используется `strider`.
```
ENV HOME /home/strider
```
Клонирование и компиляция прилодения
```
RUN git clone https://github.com/Strider-CD/strider /opt/strider/src && \
  cd /opt/strider/src && git checkout v1.11.0 && npm install
```
Открытие порта 3000.
```
EXPOSE 3000
```
Добавление пользователя `root`.
```
USER root
```
Установка рабочей диркектории
```
WORKDIR  /opt/strider/src
```
Установка пакета `jq` нужного для интеграции с `taiga`
```
RUN apt-get install -y jq
```
Установка переменной окружения, чтобы приложение работало в режиме production.
```
ENV NODE_ENV=production
```
Задание точки входа.
```
ENTRYPOINT ["npm", "start"]
```
