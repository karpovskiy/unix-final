# Описание Dockerfile для сервиса gogs

## Backend

Контейнер основан на `python:3.8`.
```
FROM python:3.8
```
Переменная `DEBIAN_FRONTEND` задана, чтобы перейти в не интерактивный режим.
```
ENV DEBIAN_FRONTEND noninteractive
```
Копирование файлов, необходимых для работы образа
```
COPY taiga-back  /usr/src/app/taiga-back/
COPY locale.gen /etc/locale.gen
COPY default.locale /etc/default/locale
```
Установка зависимостей.
```
RUN apt-get update  && apt-get autoremove -y && apt-get install locales -y
RUN locale-gen en_US.UTF-8 && dpkg-reconfigure locales
RUN apt-get install -y build-essential binutils-doc autoconf flex bison libjpeg-dev
RUN apt-get install -y libfreetype6-dev zlib1g-dev libzmq3-dev libgdbm-dev libncurses5-dev
RUN apt-get install -y automake libtool libffi-dev libssl-dev curl git tmux gettext python
```
Задание рабочей директории.
```
WORKDIR /usr/src/app/taiga-back
```
Установка `python`-зависимостей.
```
RUN pip install -r requirements.txt
```
Открытие 8000 порта
```
EXPOSE 8000
```
Определение конфигурационного-хранилища
```
VOLUME ["/usr/src/app/taiga-back/settings"]
```
Установка локализации
```
RUN locale -a
```
Задание точки входа (запуск сервера)
```
ENTRYPOINT ["python", "manage.py", "runserver", "0.0.0.0:8000"]
```

## Frontend
Конейнер основан на `nginx`
```
FROM nginx
```
Копирование необходимых для работы контейнера файлов
```
COPY taiga-front-dist /taiga
COPY run.sh /taiga/run.sh
COPY nginx.conf /etc/nginx/nginx.conf
```
Установка правил доступа к директории, задание файла исполняемым
```
RUN chmod -R 755 /taiga && chmod +x /taiga/run.sh
```
Задание конфигурационных-хранилищ
```
VOLUME ["/taiga/config",  "/etc/nginx/conf.d"]
```
Открытие 80 порта
```
EXPOSE 80
```
Задание точки входа
```
ENTRYPOINT ["/taiga/run.sh"]
```
